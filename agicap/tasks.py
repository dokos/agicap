import frappe

def hourly():
	for account in frappe.get_all("Agicap Account", filters={"disabled": 0}):
		doc = frappe.get_doc("Agicap Account", account.name)
		doc.run_method("send_new_transactions")
		doc.run_method("update_existing_transactions")
		doc.run_method("delete_cancelled_transactions")