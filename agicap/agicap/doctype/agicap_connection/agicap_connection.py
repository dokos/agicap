# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

from agicap.agicap.doctype.agicap_settings.api.connections import AgicapAPIConnections

class AgicapConnection(Document):
	def validate(self):
		if not self.connection_id:
			settings = frappe.get_single("Agicap Settings")
			api = AgicapAPIConnections(settings, self.company)
			res = api.post(data={
				"name": self.connection_name
			})

			self.connection_id = res.get("id")
