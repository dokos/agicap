# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class AgicapCompany(Document):
	def validate(self):
		if not self.dokos_company:
			if frappe.db.exists("Company", self.agicap_name):
				self.dokos_company = self.agicap_name
