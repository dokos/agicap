# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.custom.doctype.custom_field.custom_field import create_custom_field

from agicap.agicap.doctype.agicap_settings.api.companies import AgicapAPICompany

class AgicapSettings(Document):
	def validate(self):
		for dt in ["Sales Invoice", "Purchase Invoice"]:
			dfs = [
				dict(
					fieldname="agicap_id",
					label="Agicap ID",
					fieldtype="Data",
					hidden=1,
					no_copy=1
				),
				dict(
					fieldname="agicap_sync_datetime",
					label="Agicap Last Sync",
					fieldtype="Datetime",
					hidden=1,
					no_copy=1
				)
			]

			for df in dfs:
				create_custom_field(dt, df)


	@frappe.whitelist()
	def get_companies(self):
		api = AgicapAPICompany(self)
		companies = api.get()
		for company in companies:
			doc = frappe.new_doc("Agicap Company")
			doc.agicap_id = company.get("id")
			doc.agicap_name = company.get("name")

			doc.insert(ignore_mandatory=True, ignore_if_duplicate=True)



