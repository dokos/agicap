// Copyright (c) 2023, Dokos SAS and contributors
// For license information, please see license.txt

frappe.ui.form.on("Agicap Settings", {

	refresh(frm) {
		if (frm.doc.api_token && frm.doc.base_url) {
			frm.add_custom_button(__("View Companies"), () => {
				frappe.set_route("List", "Agicap Company")
			})
	
			frm.add_custom_button(__("Get companies created in Agicap"), () => {
				frappe.call({
					method: "get_companies",
					doc: frm.doc
				}).then(r => {
					frappe.show_alert({
						message: __("Companies fetched and created in Dokos. Please map them with the companies in this system."),
						indicator: "green"
					})
				})
			})
		}

		frm.trigger("add_onboarding")
	},

	add_onboarding(frm) {
		const onboarding_field = frm.get_field("onboarding")

		let message = `<h6>${__("In order to connect your Agicap Account, please follow these steps:")}</h6>`
		message += `<p>${__("1. Get your token on your Agicap dashboard: <a href='https://app.agicap.com/fr/app/parametres/openapi' target='_blank'>https://app.agicap.com/fr/app/parametres/openapi</a>")}</p>`
		message += `<p>${__("2. Get the companies created in Agicap and map them with your companies in Dokos")}</p>`
		message += `<p>${__("3. Create at least one connection between Agicap and Dokos")}</p>`
		message += `<p>${__("4. Create at least one account per currency for each connection. Your sales and purchase invoices will be pushed to this account in Agicap.")}</p>`
		message += `<p>${__("5. Activate your account in Agicap")}</p>`
		message += "<br>"

		onboarding_field.$wrapper.html(message)
	}
});
