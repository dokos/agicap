
import requests
import json

import frappe

class AgicapAPI:
	def __init__(self, settings):
		self.settings = settings
		self.token = settings.get_password("api_token")

		self.headers = {
			"Content-Type": "application/json",
			"Authorization": f"Bearer {self.token}"
		}
		self.base_url = settings.base_url
		self.session = requests.Session()

	def get(self, **kwargs):
		return self.session.get(
			self.url,
			headers=self.headers,
			params=kwargs.get("params")
		).json()

	def post(self, **kwargs):
		res = self.session.post(
			self.url,
			headers=self.headers,
			data=json.dumps(
				kwargs.get("data")
			),
			params=kwargs.get("params")
		)

		try:
			return res.json()
		except Exception:
			frappe.log_error(res.text, "POST Response error")
			return res

	def patch(self, **kwargs):
		return self.session.put(
			self.url,
			headers=self.headers,
			data=json.dumps(
				kwargs.get("data")
			),
			params=kwargs.get("params")
		).json()

	def delete(self):
		return self.session.delete(
			self.url,
			headers=self.headers
		)