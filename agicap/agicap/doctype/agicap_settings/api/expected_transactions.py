from agicap.agicap.doctype.agicap_settings.api.api import AgicapAPI

class AgicapAPIExpectedTransactions(AgicapAPI):
	def __init__(self, settings, company, account):
		super(AgicapAPIExpectedTransactions, self).__init__(settings)
		self.url = f"{self.base_url.rstrip('/')}/api/accounts/{account}/expectedtransactions"
		self.headers.update({
			"CompanyId": company
		})

	def delete_one(self, id):
		self.url = self.url + f"/{id}"
		return self.delete()

	def update_one(self, id, data):
		self.url = self.url + f"/{id}"
		return self.patch(params={
			"data": data
		})

	def push_batch(self, transactions):
		self.url = self.url + "/batch"
		return self.post(data=transactions)

	def push_one(self, transaction):
		self.url = self.url + "/one"
		return self.post(data=transaction)