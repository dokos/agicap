from agicap.agicap.doctype.agicap_settings.api.api import AgicapAPI

class AgicapAPIAccount(AgicapAPI):
	def __init__(self, settings, company, connection):
		super(AgicapAPIAccount, self).__init__(settings)
		self.url = f"{self.base_url.rstrip('/')}/api/connections/{connection}/accounts"
		self.headers.update({
			"CompanyId": company
		})