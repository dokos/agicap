from agicap.agicap.doctype.agicap_settings.api.api import AgicapAPI

class AgicapAPICompany(AgicapAPI):
	def __init__(self, settings):
		super(AgicapAPICompany, self).__init__(settings)
		self.url = f"{self.base_url.rstrip('/')}/api/companies"