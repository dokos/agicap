from agicap.agicap.doctype.agicap_settings.api.api import AgicapAPI

class AgicapAPIConnections(AgicapAPI):
	def __init__(self, settings, company):
		super(AgicapAPIConnections, self).__init__(settings)
		self.url = f"{self.base_url.rstrip('/')}/api/connections"
		self.headers.update({
			"CompanyId": company
		})