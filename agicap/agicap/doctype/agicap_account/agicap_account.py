# Copyright (c) 2023, Dokos SAS and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document
from frappe.utils import now_datetime
from frappe.query_builder import Column

from agicap.agicap.doctype.agicap_settings.api.accounts import AgicapAPIAccount
from agicap.agicap.doctype.agicap_settings.api.expected_transactions import AgicapAPIExpectedTransactions

class AgicapAccount(Document):
	def validate(self):
		if not self.account_id:
			api = AgicapAPIAccount(self.agicap_settings, self.company, self.connection_id)
			res = api.post(data={
				"name": self.account_name,
				"isoCurrency": self.currency
			})

			self.account_id = res.get("id")

	@property
	def connection_id(self):
		return frappe.db.get_value("Agicap Connection", self.connection, "connection_id")

	@property
	def agicap_settings(self):
		return frappe.get_single("Agicap Settings")

	def get_expected_transactions(self):
		api = AgicapAPIExpectedTransactions(self.agicap_settings, self.company, self.account_id)
		return api.get()

	def send_new_transactions(self):
		"""
		Send a batch of expected transactions to Agicap.
		External transactions can be of type:
		- Sales Invoice
		- Purchase Invoice

		Each transaction is mapped with a object with the following keys:
		{
			"amount": float
			"isCashInflow": bool
			"externalReference": str | None
			"name": str
			"paymentDate": datetime
			"billingDate": str
			"metadata": {} | None
		}
		"""
		expected_transactions = {
			"Sales Invoice": self.get_sales_invoices(),
			"Purchase Invoice": self.get_purchase_invoices()
		}

		api = AgicapAPIExpectedTransactions(self.agicap_settings, self.company, self.account_id)

		for transaction_type in expected_transactions:
			if expected_transactions[transaction_type]:
				res =  api.push_batch(
					expected_transactions[transaction_type]
				)

				for data in res:
					if data.get("name"):
						frappe.db.set_value(transaction_type, data.get("name"), "agicap_id", data.get("id"))
						frappe.db.set_value(transaction_type, data.get("name"), "agicap_sync_datetime", now_datetime())

	def update_existing_transactions(self):
		filters = {
			"agicap_id": ("is", "set"),
			"modified": (">", Column("agicap_sync_datetime"))
		}

		updated_transactions = {
			"Sales Invoice": self.get_sales_invoices(filters),
			"Purchase Invoice": self.get_purchase_invoices(filters)
		}

		api = AgicapAPIExpectedTransactions(self.agicap_settings, self.company, self.account_id)

		for transaction_type in updated_transactions:
			if updated_transactions[transaction_type]:
				res = api.push_batch(
					updated_transactions[transaction_type]
				)

				for data in res:
					if data.get("name"):
						frappe.db.set_value(transaction_type, data.get("name"), "agicap_sync_datetime", now_datetime())


	def delete_cancelled_transactions(self):
		filters = {
			"agicap_id": ("is", "set"),
			"docstatus": 2
		}

		cancelled_transactions = {
			"Sales Invoice": self.get_transactions("Sales Invoice", filters),
			"Purchase Invoice": self.get_transactions("Purchase Invoice", filters)
		}

		api = AgicapAPIExpectedTransactions(self.agicap_settings, self.company, self.account_id)

		for transaction_type in cancelled_transactions:
			for transaction in cancelled_transactions[transaction_type]:
				res = api.delete_one(transaction["agicap_id"])
				if res.status_code in (200, 404):
					frappe.db.set_value(transaction_type, transaction["name"], "agicap_id", None)
					frappe.db.set_value(transaction_type, transaction["name"], "agicap_sync_datetime", now_datetime())


	def get_sales_invoices(self, filters=None):
		transactions = self.get_transactions("Sales Invoice", filters)

		result = []
		for transaction in transactions:
			result.append({
				"amount": transaction.grand_total,
				"isCashInflow": True,
				"externalReference": transaction.external_reference,
				"name": transaction.name,
				"paymentDate": transaction.due_date.isoformat(),
				"billingDate": transaction.posting_date.isoformat()
			})

		return result

	def get_purchase_invoices(self, filters=None):
		transactions = self.get_transactions("Purchase Invoice", filters)

		result = []
		for transaction in transactions:
			result.append({
				"amount": transaction.grand_total,
				"isCashInflow": False,
				"externalReference": transaction.bill_no,
				"name": transaction.name,
				"paymentDate": transaction.due_date.isoformat(),
				"billingDate":  transaction.posting_date.isoformat()
			})

		return result


	def get_transactions(self, transaction_type, filters=None):
		# TODO: handle different accounts in different currencies

		query_filters = {
			"docstatus": 1,
			"company": frappe.db.get_value("Agicap Company", self.company, "dokos_company"),
			"agicap_id": ("is", "not set")
		}

		if filters:
			query_filters.update(filters)

		fields = ["name", "base_grand_total", "currency", "due_date", "posting_date", "agicap_id"]
		if transaction_type == "Sales Invoice":
			fields.append("external_reference")

		elif transaction_type == "Purchase Invoice":
			fields.append("bill_no")

		return frappe.get_all(transaction_type, filters=query_filters, fields=fields)
